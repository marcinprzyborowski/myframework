<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 27.07.17
 * Time: 18:45
 */

namespace tests\Engine\Http;


use Engine\Http\Request;
use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase
{

    /**
     * @var Request
     */
    private $request;

    /**
     * @var array
     */
    private $params;

    /**
     * @var array
     */
    private $server;

    protected function setUp()
    {
        $this->params = self::getParams();

        $this->server = [
            'REQUEST_URI' => "dummy data"
        ];

        $this->request = new Request($this->params, $this->server);
    }

    public static function getParams()
    {
        return [
            'param1' => 1,
            'param2' => "text",
            'param3' => ["table" => "table"],
            'param4' => null,
        ];
    }

    public function testGetRequestUri()
    {
        $result = $this->request->getRequestURI();
        $expected = $this->server['REQUEST_URI'];

        $this->assertEquals($expected, $result);
    }


    public function testGetParams()
    {
        $result = $this->request->getParams();

        $this->assertEquals($this->params, $result);
    }

    /**
     * @dataProvider getDataProvider
     * @param $param
     * @param $expected
     */
    public function testGet($param, $expected)
    {
        $result = $this->request->get($param);

        $this->assertEquals($expected, $result);
    }

    public function getDataProvider()
    {
        $data = [];
        $data[] = ["param5", null];
        $params = self::getParams();
        foreach ($params as $param => $value) {
            $data[] = [$param, $value];
        }

        return $data;
    }
}