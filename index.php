<?php

require ('vendor/autoload.php');

$kernel = new \Engine\Kernel("dev");

$request = \Engine\Http\RequestBuilder::build();
$response = $kernel->handleRequest($request);

print $kernel->handleResponse($response);
