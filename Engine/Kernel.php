<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 12.07.17
 * Time: 20:10
 */

namespace Engine;

use Engine\Configuration\Config;
use Engine\Http\Request;
use Engine\Http\Response;
use Engine\Router\Router;

class Kernel
{
    private $env;

    function __construct($env = "prod")
    {
        $this->env = $env;
        $this->init();
    }

    private function init()
    {
        if ($this->env == "dev") {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
        }
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \HttpException
     */
    public function handleRequest(Request $request) : Response
    {
        $router = new Router();
        $response = $router->resolve($request);

        if ($response instanceof Response) {
            return $response;
        }

        throw new \HttpException("Controller must return response object");
    }

    /**
     * @param Response $response
     * @return string
     */
    public function handleResponse(Response $response)
    {
        http_response_code($response->getStatusCode());
        return $response->getContent();
    }
}