<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 12.07.17
 * Time: 21:01
 */

namespace Engine\Configuration;


use Engine\Configuration\Validator\ConfigValidator;

/**
 * Class Config
 * @package Engine\Configuration
 */
class Config
{
    /**
     * @var array
     */
    private $configCollection;
    /**
     * @var
     */
    private static $instance;
    /**
     * @var array
     */
    private static $configFile =
        [
            "alias" => "config",
            "fileName" => "config.json",
        ];

    /**
     * do not allow to clone object
     */
    private function __clone()
    {
    }

    /**
     * Config constructor.
     */
    private function __construct()
    {
        $this->configCollection = [];
        $this->loadConfiguration();
    }

    /**
     *  initialize config files
     */
    private function loadConfiguration()
    {
        $configFile = self::$configFile;
        $this->loadConfigurationFile($configFile);
        $this->loadConfigurationImports();
    }

    /**
     * @param array $config
     * @throws \Exception
     */
    private function loadConfigurationFile(array $config = [])
    {
        $this->validConfig($config);

        $alias = $config['alias'];
        $fileName = $config['fileName'];

        $file = __DIR__ . "/../../Config/$fileName";

        /** @var string $content */
        $content = file_get_contents($file);

        if (empty($content)) {
            throw new \Exception("file $fileName doesn't exist");
        }

        $this->configCollection[$alias] = json_decode($content,true);
    }

    /**
     * @param $config
     * @return bool
     */
    private function validConfig($config)
    {
        return ConfigValidator::validConfig($config);
    }

    /**
     * load importing files
     */
    private function loadConfigurationImports()
    {
        $config = $this->get('config');

        if (!isset($config['imports'])) {
            return;
        }

        foreach ($config['imports'] as $import) {
            $this->loadConfigurationFile($import);
        }
    }

    /**
     * @return Config
     */
    public static function getInstance(): Config
    {
        if (!self::$instance) {
            self::$instance = new Config();
        }

        return self::$instance;
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function get($name) : array
    {
        if (!isset($this->configCollection[$name])) {
            throw new \Exception("Configuration doesn't exist");
        }

        return $this->configCollection[$name];
    }


}