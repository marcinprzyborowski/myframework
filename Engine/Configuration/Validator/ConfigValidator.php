<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 12.07.17
 * Time: 21:32
 */

namespace Engine\Configuration\Validator;


class ConfigValidator
{
    private static $configStructure = [
        "alias",
        "fileName"
    ];

    /**
     * @param array $array
     * @return bool
     * @throws \Exception
     */
    public static function validConfig(array $array = []): bool
    {
        foreach ($array as $key => $value) {
            if (in_array($key, self::$configStructure)) {
                continue;
            }

            throw new \Exception("config file is not valid");
        }

        return true;
    }


}