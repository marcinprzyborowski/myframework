<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 12.07.17
 * Time: 20:32
 */

namespace Engine\Controller;


use Engine\Http\Response;

abstract class BaseController
{

    public function Response($content = "", $statusCode = Response::HTTP_OK)
    {
        return new Response($content, $statusCode);
    }

    public function get($serviceName)
    {
        return $serviceName;
    }

}