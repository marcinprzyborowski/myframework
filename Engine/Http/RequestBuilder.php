<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 10.07.17
 * Time: 21:10
 */

namespace Engine\Http;

class RequestBuilder
{

    /**
     * @return Request
     */
    public static function build(): Request
    {
        $params = self::resolveParams();
        $server = self::resolveServer();
        $request = new Request($params, $server);
        return $request;
    }

    /**
     * @return mixed
     */
    private static function resolveParams()
    {
        $params = $_REQUEST;
        return $params;
    }

    private static function resolveServer()
    {
        $server = $_SERVER;
        return $server;
    }


}