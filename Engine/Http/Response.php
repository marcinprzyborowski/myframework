<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 12.07.17
 * Time: 19:55
 */

namespace Engine\Http;


class Response
{

    const HTTP_OK = 200;

    /**
     * @var string
     */
    private $content;

    /**
     * @var int
     */
    private $statusCode;

    function __construct($content = "", $statusCode = self::HTTP_OK)
    {
        $this->content = $content;
        $this->statusCode = $statusCode;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Response
     */
    public function setContent(string $content): Response
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     * @return Response
     */
    public function setStatusCode(int $statusCode): Response
    {
        $this->statusCode = $statusCode;
        return $this;
    }


}