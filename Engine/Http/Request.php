<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 10.07.17
 * Time: 20:33
 */

namespace Engine\Http;


class Request
{

    private $params;
    private $server;

    /**
     * Request constructor.
     * @param array $params
     * @param array $server
     */
    function __construct(array $params, array $server)
    {
        $this->params = $params;
        $this->server = $server;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param $param
     * @param null $default
     * @return mixed
     */
    public function get($param, $default = null)
    {
        return $this->resolveParam($param, $default);
    }

    /**
     * @param $param
     * @param $default
     * @return mixed
     */
    private function resolveParam($param, $default)
    {
        $result = $default;
        if (isset($this->params[$param])) {
            $result = $this->params[$param];
        }
        return $result;
    }

    /**
     * @return mixed
     */
    public function getRequestURI()
    {
        return $this->server['REQUEST_URI'];
    }

}