<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 27.07.17
 * Time: 19:42
 */

namespace Engine\Router;


use Engine\Exceptions\RouterException;
use Engine\Http\Request;

class Router
{

    /**
     * @var RouteMatcher
     */
    private $matcher;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Route
     */
    private $route;


    public function __construct()
    {
        $this->matcher = new RouteMatcher();
    }

    public function resolve(Request $request)
    {
        $this->request = $request;
        $uri = $this->request->getRequestURI();
        $this->route = $this->matcher->match($uri);

        return $this->execute();
    }

    /**
     * @return mixed
     */
    private function execute()
    {
        $object = $this->buildObject();
        $result = $this->callMethod($object);

        return $result;
    }

    /**
     * @return mixed
     * @throws RouterException
     */
    private function buildObject()
    {
        $className = $this->route->getClassName();
        if (!class_exists($className)) {
            throw new RouterException();
        }

        $class = new $className;
        return $class;
    }

    /**
     * @param $object
     * @return mixed
     * @throws RouterException
     */
    private function callMethod($object)
    {
        $method = $this->route->getClassMethod();
        if (!method_exists($object, $method)) {
            throw new RouterException();
        }

        $result = $object->{$method}($this->request);
        return $result;
    }


}