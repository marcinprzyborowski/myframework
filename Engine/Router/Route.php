<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 12.07.17
 * Time: 20:46
 */

namespace Engine\Router;


class Route
{
    private $name = "";
    private $path = "";
    private $className = "";
    private $classMethod = "";
    private $httpMethod = "";

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Route
     */
    public function setName(string $name): Route
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return Route
     */
    public function setPath(string $path): Route
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @param string $className
     * @return Route
     */
    public function setClassName(string $className): Route
    {
        $this->className = $className;
        return $this;
    }

    /**
     * @return string
     */
    public function getClassMethod(): string
    {
        return $this->classMethod;
    }

    /**
     * @param string $classMethod
     * @return Route
     */
    public function setClassMethod(string $classMethod): Route
    {
        $this->classMethod = $classMethod;
        return $this;
    }

    /**
     * @return string
     */
    public function getHttpMethod(): string
    {
        return $this->httpMethod;
    }

    /**
     * @param string $httpMethod
     * @return Route
     */
    public function setHttpMethod(string $httpMethod): Route
    {
        $this->httpMethod = $httpMethod;
        return $this;
    }



}