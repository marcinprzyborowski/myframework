<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 27.07.17
 * Time: 19:42
 */

namespace Engine\Router;


use Engine\Configuration\Config;

class RouteMatcher
{

    /**
     * @var Config
     */
    private $config;

    /**
     * RouteMatcher constructor.
     */
    public function __construct()
    {
        $this->config = Config::getInstance();
    }


    /**
     * @param $path
     * @return Route
     * @throws \Exception
     */
    public function match($path)
    {
        $routing = $this->getRouting();

        foreach ($routing as $item) {
            $uri = $item['path'];
            if ($path == $uri) {
                return $this->buildRoute($item);
            }
        }

        throw new \Exception('Route not found');
    }

    /**
     * @return array|mixed
     */
    private function getRouting()
    {
        $routing = $this->config->get("routing");
        return $routing;
    }

    /**
     * @param array $item
     * @return Route
     */
    private function buildRoute(array $item)
    {
        $route = (new Route)
            ->setName($item['name'])
            ->setPath($item['path'])
            ->setClassName($item['className'])
            ->setClassMethod($item['classMethod'])
            ->setHttpMethod($item['httpMethod']);

        return $route;
    }

}