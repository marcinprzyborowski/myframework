<?php
/**
 * Created by PhpStorm.
 * User: marcin
 * Date: 10.07.17
 * Time: 21:41
 */

namespace Controller;

use Engine\Controller\BaseController;
use Engine\Http\Request;

class BasicController extends BaseController
{
    function indexAction(Request $request)
    {
        $content = $request->get("abc", "string");

        return $this->Response($content);
    }
}